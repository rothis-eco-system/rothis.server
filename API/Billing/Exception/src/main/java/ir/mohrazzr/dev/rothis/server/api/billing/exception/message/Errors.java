package ir.mohrazzr.dev.rothis.server.api.billing.exception.message;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 11:08 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Errors
 **/

public enum Errors {
    MISSING_REQUIRED_FIELD("Missing required filed. check documentation for required fields "),
    NO_RECORD_FOUND("No record found "),
    NO_USER_FOUND_WITH_ID("No user found with this id : "),
    NO_USER_FOUND_WITH_USERNAME("No user found with this user name : "),
    RECORD_ALREADY_EXISTS("Record already exists "),
    INTERNAL_SERVER_ERROR("Internal server error "),
    AUTHENTICATION_FIELD("Authentication field "),
    COULD_NOT_UPDATE_RECORD("Could not update record "),
    COULD_NOT_DELETE_RECORD("Could not delete record "),
    USER_NOT_VERIFIED("User not verified "),
    USER_NAME_EXISTS("User name already exists! "),
    EMAIL_ADDRESS_EXISTS("Email Address already exists! "),
    DELETE_RECORD_SUCCESSFULLY("Delete record successfully"),
    NO_USER_FOUND_WITH_PUBLIC_USER_ID("No user found with this public user id : "),
    NO_USER_FOUND_WITH_EMAIL("No user found with this email :"),
    NO_USER_ROLE_FOUND("No user role found!"),
    COULD_NOT_INSERT_RECORD("Could not insert record"),
    NOT_ALLOW_TO_TAKE_USER_ROLE("You are not allow take this role to your users."),
    USER_ID_HAS_TAKEN("This user id has taken before or it's was wrong id"),
    NO_PERSON_FOUND_WITH_PUBLIC_USER_ID("No person found with this public user id :"),
    NOT_FOUND_PERSONAL_INFORMATION_CREATE_NEW("Not found person information's! please fill personal information's for this user"),
    NOT_FOUND_PERSON_ADDRESS("Not found person address, this person did'nt have any addresses."),
    THIS_NATIONAL_CODE_HAS_STORED_BEFORE("This national code has stored before :"),
    NOT_FOUND_PERSON_PHONE_NUMBER("Not found person phone number, this person did'nt have any phone number."),
    NO_PERMISSION_TO_CREATE_RESOURCE("You dont have a permission to create a resource!"),
    RESOURCE_ALREADY_EXIST("Resource already exist with this identifier code :"),
    NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER("Not found resource with public id and manager id."),
    TOTAL_UNALLOCATED_RESOURCE_PORTION_IS_ZERO("Total unallocated resource portion is zero."),
    PORTION_OWNER_IS_DUPLICATED_IN_THIS_RESOURCE("Portion owner is duplicated in this resource please check and update portion owner if it does exists"),
    NOT_FOUND_PORTION_OWNER_IN_THIS_RESOURCE("Not found portion owner in this resource"),
    NOT_FOUND_PORTION_WITH_THIS_PUBLIC_ID("Not found portion with this public id :"),
    YOUR_NOT_ADMIN_OF_THIS_RESOURCE("Your not admin of this resource or resource not found."),
    NOT_FOUND_BANK_ACCOUNT_WHIT_THIS_PUBLIC_ID("Not found bank account with this public id :"),
    BANK_ACCOUNT_OWNER_ALREADY_EXISTS("Bank account owner already exists with this public user id :"),
    BANK_ACCOUNT_ALREADY_EXISTS("Bank account already exists with this account number : "),
    NOT_FOUND_BANK_ACCOUNT_WHIT_THIS_PUBLIC_ID_OR_RESOURCE_ID("Not found bank account with public id or not belong to this resource.");



    private String message;
    Errors(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }}

