package ir.mohrazzr.dev.rothis.server.api.billing.exception.handler;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 11:10 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UserServiceException1
 **/

public class RothisExceptionService extends RuntimeException {
    private static final long serialVersionUID = -1790004921736815097L;

    public RothisExceptionService(String message) {
        super(message);
    }
}
