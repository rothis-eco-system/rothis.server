package ir.mohrazzr.dev.rothis.server.api.billing.exception.message;

import java.util.Date;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 11:09 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ErrorMessage
 **/

public class ErrorMessage {
    private Date timestamp;
    private String message;

    public ErrorMessage() {
    }
    public ErrorMessage(Date timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
