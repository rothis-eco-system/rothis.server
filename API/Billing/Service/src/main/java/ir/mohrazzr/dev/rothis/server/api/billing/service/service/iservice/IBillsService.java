package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BillsDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/12/01 - 4:29 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IBillsService
 **/

public interface IBillsService {
    List<BillsDTO> createBills(String resourcePublicId, String mangerPublicId, String resourceBillsPublicId);
    List<BillsDTO> updateBills(String resourcePublicId, String managerPublicId, String resourceBillsPublicId);
    List<BillsDTO> readAllBills(String resourcePublicId, String managerPublicId, String resourceBillsPublicId);
}
