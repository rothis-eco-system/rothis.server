package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PortionOwnersDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/16 - 4:52 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IPortionOwnersService
 **/

public interface IPortionOwnersService {
    PortionOwnersDTO createPortion(PortionOwnersDTO inDTO, String publicUserId, String publicResourceId, String ownerPublicUserId);
    PortionOwnersDTO readPortion(String ownerPublicUserId, String publicResourceId, String publicPortionId);
    List<PortionOwnersDTO> readAllPortion(String publicResourceId, String managerPublicUserId, int page, int limit);
    PortionOwnersDTO updatePortion(PortionOwnersDTO inDTO, String publicUserId, String publicResourceId, String ownerPublicUserId, String portionPublicId);
    PortionOwnersDTO findById(Long id);
    OperationStatusDTO deletePortion(String managerPublicUserId, String publicResourceId, String ownerPublicUserId, String portionPublicId);
    int countByResourceId(Long id);
}
