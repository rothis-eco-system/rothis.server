package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PhoneNumbersDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/09 - 4:50 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IPhoneNumbersService
 **/

public interface IPhoneNumbersService {
    PhoneNumbersDTO createPhoneNumber(PhoneNumbersDTO inDTO, String publicUserId);
    PhoneNumbersDTO createPhoneNumber(PhoneNumbersDTO inDTO, Long personId);
    PhoneNumbersDTO readPhoneNumber(String publicUserId, String phoneNumberPublicId);
    PhoneNumbersDTO updatePhoneNumber(PhoneNumbersDTO inDTO, String publicUserId, String phoneNumberPublicId);
    OperationStatusDTO deletePhoneNumber(String publicUserId, String publicId);
    List<PhoneNumbersDTO> readAllPhoneNumbersByPersonId(Long personId);
    List<PhoneNumbersDTO> readAllPhoneNumbersByPublicUserId(String publicUserId);
}
