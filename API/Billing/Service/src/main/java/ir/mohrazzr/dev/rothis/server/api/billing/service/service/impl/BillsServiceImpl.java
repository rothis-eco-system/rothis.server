package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BillsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.BillsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BillsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PersonsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PortionOwnersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import ir.mohrazzr.dev.rothis.server.api.utils.calculator.ResourcesCalculation;
import ir.mohrazzr.dev.rothis.server.api.utils.UnitsUtil;
import ir.mohrazzr.dev.rothis.server.api.utils.converter.Converter;
import ir.mohrazzr.dev.rothis.server.api.utils.generator.Generator;
import ir.mohrazzr.dev.rothis.server.api.utils.times.TimeUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/12/01 - 6:03 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BillsServiceImpl
 **/
@Service
public class BillsServiceImpl implements IBillsService {

    private BillsRepository billsRepository;
    private IResourceManagerService resourceManagerService;
    private IPortionOwnersService portionOwnersService;
    private IResolverService resolverService;
    private IPublicIDsService publicIDsService;
    private IPersonsService personsService;
    private IPortionOwnersService ownersService;
    private TimeUtil timeUtil;
    private Converter converter;
    private ModelMapper modelMapper;
    private Generator generator;

    @Autowired
    public BillsServiceImpl(BillsRepository billsRepository, IResourceManagerService resourceManagerService, IPortionOwnersService portionOwnersService, IResolverService resolverService, IPublicIDsService publicIDsService, IPersonsService personsService, IPortionOwnersService ownersService) {
        this.billsRepository = billsRepository;
        this.resourceManagerService = resourceManagerService;
        this.portionOwnersService = portionOwnersService;
        this.resolverService = resolverService;
        this.publicIDsService = publicIDsService;
        this.personsService = personsService;
        this.ownersService = ownersService;
        this.timeUtil = new TimeUtil();
        this.converter = new Converter();
        this.modelMapper = new ModelMapper();
        this.generator = new Generator();
    }


    @Override
    public List<BillsDTO> createBills(String resourcePublicId, String managerPublicId, String resourceBillsPublicId) {
        List<BillsDTO> billsDTOList = new ArrayList<>();
        if (resourceManagerService.ifManagerOfResource(managerPublicId, resourcePublicId)) {
            List<PortionOwnersDTO> portionOwnersDTOList = portionOwnersService.readAllPortion(resourcePublicId, managerPublicId, 1, portionOwnersService.countByResourceId(resolverService.getResourceID(resourcePublicId)) + 5);
            System.out.println(Arrays.toString(portionOwnersDTOList.toArray()));

            BigDecimal pricePerSeconds = resolverService.getResourceBillPricePerSeconds(resourcePublicId, resourceBillsPublicId);
            if (pricePerSeconds.compareTo(BigDecimal.ZERO) != 0) {
                int count = 1;
                for (PortionOwnersDTO portionOwnersDTO : portionOwnersDTOList) {
                    BillsDTO billsDTO = new BillsDTO();
                    BillsEntity billsEntity = new BillsEntity();
                    PublicIDsDTO billPublicIDsDTO = publicIDsService.generatePublicId(40);
                    billsDTO.setPublicId(billPublicIDsDTO.getId());
                    billsDTO.setPublicIdString(billPublicIDsDTO.getPublicId());
                    billsDTO.setResourceBillsId(resolverService.getResourceBillID(resourcePublicId, resourceBillsPublicId));
                    billsDTO.setResourceBillsPublicIdString(resourceBillsPublicId);
                    billsDTO.setPortionOwnerId(portionOwnersDTO.getId());
                    PublicIDsDTO portionPublicIDsDTO = publicIDsService.findById(portionOwnersDTO.getPublicId());
                    billsDTO.setPortionOwnerPublicId(portionPublicIDsDTO.getPublicId());
                    billsDTO.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                    timeUtil.setTimestamp(billsDTO.getCreatedAt());
                    billsDTO.setCreatedAtPersianDate(timeUtil.convertTimestampToPersianDate().getPersianDate().toString());
                    billsDTO.setBillPrice(ResourcesCalculation.getPortionOwnerTotalPrice(pricePerSeconds, portionOwnersDTO.getPortionSeconds()));
                    billsDTO.setBillIdentityCode(generator.generateRandomNumber(9));
                    billsDTO.setBillNumber(count++);
                    billsDTO.setBillPriceInAlphabet(converter.getParsedString(billsDTO.getBillPrice().toString()).concat(" ").concat("ریال"));
                    modelMapper.map(billsDTO, billsEntity);
                    BillsEntity resultBillsEntity = billsRepository.save(billsEntity);
                    if (resultBillsEntity != null) {
                        billsDTO.setBillPrice(UnitsUtil.fixDecimalValue(billsEntity.getBillPrice()));
                        billsDTO.setBillPriceInAlphabet(converter.getParsedString(String.valueOf(UnitsUtil.fixDecimalValue(billsDTO.getBillPrice()))).concat(" ").concat("ریال"));
                        billsDTOList.add(billsDTO);
                    } else {
                        publicIDsService.deletePublicId(billPublicIDsDTO.getId());
                        throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage());
                    }
                }
                return billsDTOList;
            } else
                throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + " : Price per seconds is null");
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public List<BillsDTO> updateBills(String resourcePublicId, String managerPublicId, String resourceBillsPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicId, resourcePublicId)) {

            List<BillsEntity> billsEntityList = billsRepository.findAllByResourceBillsId(resolverService.getResourceBillID(resourcePublicId, resourceBillsPublicId));
            for (BillsEntity billsEntity : billsEntityList) {
                billsRepository.delete(billsEntity);
            }
            return createBills(resourcePublicId, managerPublicId, resourceBillsPublicId);
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public List<BillsDTO> readAllBills(String resourcePublicId, String managerPublicId, String resourceBillsPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicId, resourcePublicId)) {
            List<BillsEntity> billsEntityList = billsRepository.findAllByResourceBillsId(resolverService.getResourceBillID(resourcePublicId, resourceBillsPublicId));
            List<BillsDTO>billsDTOList = new ArrayList<>();
            for (BillsEntity billsEntity : billsEntityList) {
                ModelMapper modelMapper = new ModelMapper();
                BillsDTO billsDTO = new BillsDTO();
                modelMapper.map(billsEntity, billsDTO);
                billsDTO.setPublicIdString(publicIDsService.findById(billsEntity.getPublicId()).getPublicId());
                billsDTO.setResourceBillsPublicIdString(publicIDsService.findById(billsEntity.getResourceBillsId()).getPublicId());
                billsDTO.setPortionOwnerPublicId(publicIDsService.findById(billsEntity.getPortionOwnerId()).getPublicId());
                PortionOwnersDTO portionOwnersDTO = ownersService.findById(billsEntity.getPortionOwnerId());
                PersonsDTO personsDTO = personsService.findById(portionOwnersDTO.getPersonId());
                billsDTO.setOwner(personsDTO);
                timeUtil.setTimestamp(billsDTO.getCreatedAt());
                billsDTO.setBillPrice(UnitsUtil.fixDecimalValue(billsEntity.getBillPrice()));
                billsDTO.setCreatedAtPersianDate(timeUtil.convertTimestampToPersianDate().getPersianDate().toString());
                billsDTO.setBillPriceInAlphabet(converter.getParsedString(String.valueOf(UnitsUtil.fixDecimalValue(billsEntity.getBillPrice()))).concat(" ").concat("ریال"));
                billsDTOList.add(billsDTO);
            }
            return billsDTOList;
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }
}
