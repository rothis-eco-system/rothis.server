package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.AddressesDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/09 - 4:53 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IAddressesService
 **/

public interface IAddressesService {
    AddressesDTO createAddress(AddressesDTO inDTO, Long personId);
    AddressesDTO createAddress(AddressesDTO inDTO, String personId);
    AddressesDTO readAddress(String publicUserId, String addressId);
    AddressesDTO updateAddress(AddressesDTO inDTO, String publicUserId, String publicAddressId);
    OperationStatusDTO deleteAddress(String publicUserId, String publicId);
    List<AddressesDTO> readAllAddressesByPersonId(Long personId);

    List<AddressesDTO> readAllAddressesByPublicUserId(String publicUserId);
}
