package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PhoneNumbersEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.PhoneNumbersRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PhoneNumbersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PublicIDsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPhoneNumbersService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPublicIDsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResolverService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/09 - 5:01 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PhoneNumberServiceImpl
 **/
@Service
public class PhoneNumbersServiceImpl implements IPhoneNumbersService {

    private PhoneNumbersRepository phoneNumbersRepository;
    private IPublicIDsService publicIDsService;
    private IUsersService usersService;
    private IResolverService idResolverService;

    @Autowired
    public PhoneNumbersServiceImpl(PhoneNumbersRepository PhoneNumbersRepository, IPublicIDsService publicIDsService, IUsersService usersService, IResolverService idResolverService) {
        this.phoneNumbersRepository = PhoneNumbersRepository;
        this.publicIDsService = publicIDsService;
        this.usersService = usersService;
        this.idResolverService = idResolverService;
    }

    @Override
    public PhoneNumbersDTO createPhoneNumber(PhoneNumbersDTO inDTO, Long personId) {
        return getPhoneNumbersDTO(inDTO, personId);
    }

    @Override
    public PhoneNumbersDTO createPhoneNumber(PhoneNumbersDTO inDTO, String personPublicId) {
        return getPhoneNumbersDTO(inDTO, idResolverService.getPersonID(personPublicId));
    }

    @Override
    public PhoneNumbersDTO readPhoneNumber(String publicUserId, String publicPhoneNumberId) {
        PhoneNumbersDTO phoneNumbersDTO = new PhoneNumbersDTO();
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicPhoneNumberId);
        PhoneNumbersEntity phoneNumbersEntity = phoneNumbersRepository.findByPersonsIdAndPublicId(idResolverService.getPersonID(publicUserId), publicIDsDTO.getId());
        BeanUtils.copyProperties(phoneNumbersEntity, phoneNumbersDTO);
        phoneNumbersDTO.setPublicPhoneNumberId(publicIDsDTO.getPublicId());
        return phoneNumbersDTO;
    }

    @Override
    public PhoneNumbersDTO updatePhoneNumber(PhoneNumbersDTO inDTO, String publicUserId,String publicPhoneNumberId) {
        PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicPhoneNumberId);
        Long personId =idResolverService.getPersonID(publicUserId);
        PhoneNumbersEntity foundedPhoneNumbersEntity = phoneNumbersRepository.findByPersonsIdAndPublicId(personId, publicIDsDTO.getId());
        PhoneNumbersEntity resultPhoneNumbersEntity = new PhoneNumbersEntity();
        if (foundedPhoneNumbersEntity != null) {
            PublicIDsDTO updatedPublicIDsDTO = publicIDsService.updatePublicId(publicIDsDTO.getPublicId());
            foundedPhoneNumbersEntity.setPublicId(updatedPublicIDsDTO.getId());
            foundedPhoneNumbersEntity.setIsPrimary(inDTO.getPrimary());
            foundedPhoneNumbersEntity.setPhoneNumber(inDTO.getPhoneNumber());
            foundedPhoneNumbersEntity.setTypeCode(inDTO.getTypeCode());
            resultPhoneNumbersEntity = phoneNumbersRepository.save(foundedPhoneNumbersEntity);
            PhoneNumbersDTO resultPhoneNumbersDTO =new ModelMapper().map(resultPhoneNumbersEntity, PhoneNumbersDTO.class);
            resultPhoneNumbersDTO.setPublicPhoneNumberId(updatedPublicIDsDTO.getPublicId());
            return resultPhoneNumbersDTO;
        } else
            throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage() + "," + Errors.NOT_FOUND_PERSON_PHONE_NUMBER.getMessage());
    }

    @Override
    public OperationStatusDTO deletePhoneNumber(String publicUserId, String publicId) {
        if (usersService.findUserByPublicUserId(publicUserId) != null) {
            PublicIDsDTO publicIDsDTO = publicIDsService.findByPublicId(publicId);
            if (publicIDsDTO != null) {
                PhoneNumbersEntity foundedPhoneNumbersEntity = phoneNumbersRepository.findByPublicId(publicIDsDTO.getId());
                if (foundedPhoneNumbersEntity != null) {
                    phoneNumbersRepository.delete(foundedPhoneNumbersEntity);
                    return new OperationStatusDTO(OperationName.DELETE.name(),
                            OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                            Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
                }
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public List<PhoneNumbersDTO> readAllPhoneNumbersByPersonId(Long personId) {
        List<PhoneNumbersDTO> phoneNumbersDTOList = new ArrayList<>();
        List<PhoneNumbersEntity> phoneNumbersEntityList = phoneNumbersRepository.findAllByPersonsId(personId);
        for (PhoneNumbersEntity phoneNumbersEntity : phoneNumbersEntityList) {
            PublicIDsDTO phoneNumbersPublicIDsDTO = publicIDsService.findById(phoneNumbersEntity.getPublicId());
            PhoneNumbersDTO phoneNumbersDTO = new PhoneNumbersDTO();
            BeanUtils.copyProperties(phoneNumbersEntity, phoneNumbersDTO);
            phoneNumbersDTO.setPublicPhoneNumberId(phoneNumbersPublicIDsDTO.getPublicId());
            phoneNumbersDTOList.add(phoneNumbersDTO);
        }
        return phoneNumbersDTOList;
    }

    @Override
    public List<PhoneNumbersDTO> readAllPhoneNumbersByPublicUserId(String publicUserId) {
        return readAllPhoneNumbersByPersonId(idResolverService.getPersonID(publicUserId));
    }

    private PhoneNumbersDTO getPhoneNumbersDTO(PhoneNumbersDTO inDTO, Long personId) {
        PhoneNumbersDTO resultPhoneNumbersDTO = new PhoneNumbersDTO();
        PhoneNumbersEntity phoneNumbersEntity = new PhoneNumbersEntity();
        PublicIDsDTO publicIDsDTO = publicIDsService.generatePublicId(10);
        BeanUtils.copyProperties(inDTO, phoneNumbersEntity);
        phoneNumbersEntity.setPersonsId(personId);
        phoneNumbersEntity.setPublicId(publicIDsDTO.getId());
        PhoneNumbersEntity resultEntity = phoneNumbersRepository.save(phoneNumbersEntity);
        BeanUtils.copyProperties(resultEntity, resultPhoneNumbersDTO);
        resultPhoneNumbersDTO.setPublicPhoneNumberId(publicIDsDTO.getPublicId());
        return resultPhoneNumbersDTO;
    }


}
