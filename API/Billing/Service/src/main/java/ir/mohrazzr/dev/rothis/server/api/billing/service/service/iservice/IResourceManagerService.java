package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourceManagersDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/14 - 5:05 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IResourceManagerService
 **/

public interface IResourceManagerService {
    ResourceManagersDTO createManager(String publicUserId, String publicResourceId);
    ResourceManagersDTO findResourceByManagerId(Long personId, Long resourceId);
    List<Long> findAllResourceIdByManagerId(String publicUserId);
    Boolean ifManagerOfResource(String managerPublicId, String resourcePublicId);
    Boolean deleteResource(ResourceManagersDTO resourceManagersDTO);
}
