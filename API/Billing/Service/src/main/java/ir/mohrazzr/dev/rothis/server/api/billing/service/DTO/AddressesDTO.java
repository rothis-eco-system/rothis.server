package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 7:19 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AddressesDTO
 **/

public class AddressesDTO implements Serializable {
    private static final long serialVersionUID = 1196006814303618098L;
    private Long id;
    private Long personsId;
    private Long publicId;
    private String publicAddressId;
    private Boolean isPrimary;
    private Integer cityCode;
    private String postalCode;
    private String address;


    public AddressesDTO() {
    }

    public AddressesDTO(Long id, Long personsId, Long publicId, String publicAddressId, Boolean isPrimary, Integer cityCode, String postalCode, String address) {
        this.id = id;
        this.publicId = publicId;
        this.publicAddressId = publicAddressId;
        this.isPrimary = isPrimary;
        this.cityCode = cityCode;
        this.postalCode = postalCode;
        this.address = address;
        this.personsId = personsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonsId() {
        return personsId;
    }

    public void setPersonsId(Long personsId) {
        this.personsId = personsId;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicAddressId() {
        return publicAddressId;
    }

    public void setPublicAddressId(String publicAddressId) {
        this.publicAddressId = publicAddressId;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "AddressesDTO{" +
                "id=" + id +
                ", personsId=" + personsId +
                ", isPrimary=" + isPrimary +
                ", cityCode=" + cityCode +
                ", postalCode='" + postalCode + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
