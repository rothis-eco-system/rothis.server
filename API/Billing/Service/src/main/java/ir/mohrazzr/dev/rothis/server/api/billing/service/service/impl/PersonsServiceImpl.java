package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PersonsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.PersonsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 10:57 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Person : mhrz.dev@gmail.com
 * Class Name          : PersonsServiceImpl
 **/
@Service
public class PersonsServiceImpl implements IPersonsService {
    private IUsersService usersService;
    private IAddressesService addressesService;
    private IPhoneNumbersService phoneNumbersService;
    private IPublicIDsService publicIDsService;
    private PersonsRepository personsRepository;

    @Autowired
    public PersonsServiceImpl(IUsersService usersService, PersonsRepository personsRepository, IAddressesService iAddressesService, IPhoneNumbersService phoneNumbersService, IPublicIDsService publicIDsService) {
        this.usersService = usersService;
        this.addressesService = iAddressesService;
        this.personsRepository = personsRepository;
        this.phoneNumbersService = phoneNumbersService;
        this.publicIDsService = publicIDsService;
    }

    @Override
    public PersonsDTO createPerson(PersonsDTO inDTO, String publicUserId) {
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        if (usersDTO != null) {
            PersonsDTO personsDTO = getPersonsDTO(inDTO, usersDTO);
            if (personsDTO != null) {
                return personsDTO;
            } else
                throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);
        } else
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);
    }

    @Override
    public PersonsDTO createPerson(PersonsDTO inDTO, Integer userRole) throws Exception {
        UsersDTO usersDTOResult = usersService.createUser(new UsersDTO(inDTO.getFirstName() + " " + inDTO.getLastName(), inDTO.getNationalCode()), userRole, true);
        PersonsDTO personsDTO = getPersonsDTO(inDTO, usersDTOResult);
        if (personsDTO != null) {
            return personsDTO;
        } else
            throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + usersDTOResult.getPublicUserIdString());
    }

    @Override
    public PersonsDTO createPerson(PersonsDTO inDTO) throws Exception {
        UsersDTO usersDTOResult = usersService.createUser(new UsersDTO(inDTO.getFirstName() + " " + inDTO.getLastName(), inDTO.getNationalCode()), true);
        PersonsDTO personsDTO = getPersonsDTO(inDTO, usersDTOResult);
        if (personsDTO != null) {
            return personsDTO;
        } else
            throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + usersDTOResult.getPublicUserIdString());
    }

    @Override
    public PersonsDTO readPerson(String publicUserId) {
        PersonsDTO personsDTO = new PersonsDTO();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        if (usersDTO != null) {
            PersonsDTO foundedPersonsDTO = findPersonByPublicUserId(usersDTO.getPublicUserIdString());
            BeanUtils.copyProperties(foundedPersonsDTO, personsDTO);
            personsDTO.setPublicUserId(publicUserId);
            personsDTO.setUserName(usersDTO.getUserName());
            personsDTO.setAddresses(getAddressesDTOS(foundedPersonsDTO.getId()));
            personsDTO.setPhoneNumbers(getPhoneNumbersDTOS(foundedPersonsDTO.getId()));
            return personsDTO;
        } else
            throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public List<PersonsDTO> readAllPersons(int page, int limit) {
        List<PersonsDTO> dtoList = new ArrayList<PersonsDTO>();
        Pageable pageable = PageRequest.of(page, limit);
        Page<PersonsEntity> personsEntityPage = personsRepository.findAll(pageable);
        List<PersonsEntity> personsEntityList = personsEntityPage.getContent();
        ModelMapper modelMapper = new ModelMapper();
        for (PersonsEntity personsEntity : personsEntityList) {
            PersonsDTO personsDTO = new PersonsDTO();
            UsersDTO usersDTO = usersService.findById(personsEntity.getUserId());
            modelMapper.map(personsEntity, personsDTO);
            personsDTO.setPublicUserId(usersDTO.getPublicUserIdString());
            personsDTO.setUserName(usersDTO.getUserName());
            personsDTO.setAddresses(getAddressesDTOS(personsDTO.getId()));
            personsDTO.setPhoneNumbers(getPhoneNumbersDTOS(personsDTO.getId()));
            dtoList.add(personsDTO);
        }
        return dtoList;
    }

    @Override
    public PersonsDTO updatePerson(PersonsDTO inDTO, String publicUserId) {
        PersonsDTO updatedPersonsDTO = new PersonsDTO();
        ModelMapper modelMapper = new ModelMapper();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        if (usersDTO != null) {
            PersonsEntity foundedPersonEntity = personsRepository.findByUserId(usersDTO.getId());
            if (foundedPersonEntity != null) {
                if (personsRepository.findByNationalCode(inDTO.getNationalCode()) !=null && personsRepository.findByNationalCode(inDTO.getNationalCode()).equals(foundedPersonEntity)) {
                    return getUpdatedPersonsDTO(inDTO, updatedPersonsDTO, modelMapper, usersDTO, foundedPersonEntity);
                } else if (personsRepository.findByNationalCode(inDTO.getNationalCode()) == null) {
                        foundedPersonEntity.setNationalCode(inDTO.getNationalCode());
                        return getUpdatedPersonsDTO(inDTO, updatedPersonsDTO, modelMapper, usersDTO, foundedPersonEntity);
                    } else
                        throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage() + "," + Errors.THIS_NATIONAL_CODE_HAS_STORED_BEFORE.getMessage() + foundedPersonEntity.getNationalCode());


            } else
                throw new RothisExceptionService(Errors.NOT_FOUND_PERSONAL_INFORMATION_CREATE_NEW.getMessage());
        } else
            throw new RothisExceptionService(Errors.NO_PERSON_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);

    }

    private PersonsDTO getUpdatedPersonsDTO(PersonsDTO inDTO, PersonsDTO updatedPersonsDTO, ModelMapper modelMapper, UsersDTO usersDTO, PersonsEntity foundedPersonEntity) {
        PublicIDsDTO updatedPublicIDsDTO = publicIDsService.updatePublicId(usersDTO.getPublicUserIdString());
        foundedPersonEntity.setFirstName(inDTO.getFirstName());
        foundedPersonEntity.setLastName(inDTO.getLastName());
        foundedPersonEntity.setFatherName(inDTO.getFatherName());
        foundedPersonEntity.setPassPortCode(inDTO.getPassPortCode());
        foundedPersonEntity.setGender(inDTO.getGender());
        foundedPersonEntity.setDateOfBirth(inDTO.getDateOfBirth());
        PersonsEntity resultPersonsEntity = personsRepository.save(foundedPersonEntity);
        modelMapper.map(resultPersonsEntity, updatedPersonsDTO);
        updatedPersonsDTO.setUserName(usersDTO.getUserName());
        updatedPersonsDTO.setPublicUserId(updatedPublicIDsDTO.getPublicId());
        return updatedPersonsDTO;
    }

    @Override
    public OperationStatusDTO deletePerson(String publicUserId) {
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        if (usersDTO != null) {
            PersonsEntity foundedPersonEntity = personsRepository.findByUserId(usersDTO.getId());
            if (foundedPersonEntity != null) {
                personsRepository.delete(foundedPersonEntity);
                return new OperationStatusDTO(OperationName.DELETE.name(),
                        OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                        Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public PersonsDTO findPersonByPublicUserId(String publicUserId) {
        PersonsDTO personsDTO = new PersonsDTO();
        UsersDTO usersDTO = usersService.findUserByPublicUserId(publicUserId);
        PersonsEntity personsEntity = personsRepository.findByUserId(usersDTO.getId());
        if (personsEntity != null) {
            BeanUtils.copyProperties(personsEntity, personsDTO);
            return personsDTO;
        } else
            throw new RothisExceptionService(Errors.NO_USER_FOUND_WITH_PUBLIC_USER_ID.getMessage() + publicUserId);
    }

    @Override
    public PersonsDTO findById(Long id) {
        PersonsDTO resultPersonsDTO = new PersonsDTO();
        Optional<PersonsEntity> personsEntity = personsRepository.findById(id);
        personsEntity.ifPresent(e -> {
            ModelMapper modelMapper = new ModelMapper();
            PersonsDTO personsDTO = new PersonsDTO();
            modelMapper.map(e, personsDTO);
            UsersDTO usersDTO = usersService.findById(e.getUserId());
            personsDTO.setUserName(usersDTO.getUserName());
            personsDTO.setPublicUserId(usersDTO.getPublicUserIdString());
            personsDTO.setAddresses(getAddressesDTOS(personsDTO.getId()));
            personsDTO.setPhoneNumbers(getPhoneNumbersDTOS(personsDTO.getId()));
            modelMapper.map(personsDTO, resultPersonsDTO);
        });
        return resultPersonsDTO;
    }

    private PersonsDTO getPersonsDTO(PersonsDTO inDTO, UsersDTO usersDTOResult) {
        PersonsEntity personsEntity = new PersonsEntity();
        PersonsDTO personsDTO = new PersonsDTO();
        if (usersDTOResult != null) {
            inDTO.setUserId(usersDTOResult.getId());
            ModelMapper modelMapper = new ModelMapper();
            if (personsRepository.findByUserId(usersDTOResult.getId()) == null) {
                modelMapper.map(inDTO, personsEntity);
                PersonsEntity resultPersonEntity = personsRepository.save(personsEntity);
                modelMapper.map(resultPersonEntity, personsDTO);
                if (inDTO.getAddresses() != null) {
                    for (int i = 0; i < inDTO.getAddresses().size(); i++) {
                        AddressesDTO inAddressesDTO = inDTO.getAddresses().get(i);
                        AddressesDTO addressesDTO = addressesService.createAddress(inAddressesDTO, resultPersonEntity.getId());
                        inDTO.getAddresses().set(i, addressesDTO);
                    }
                }
                if (inDTO.getPhoneNumbers() != null) {
                    for (int i = 0; i < inDTO.getPhoneNumbers().size(); i++) {
                        PhoneNumbersDTO inPhoneNumbersDTO = inDTO.getPhoneNumbers().get(i);
                        PhoneNumbersDTO phoneNumbersDTO = phoneNumbersService.createPhoneNumber(inPhoneNumbersDTO, resultPersonEntity.getId());
                        inDTO.getPhoneNumbers().set(i, phoneNumbersDTO);
                    }
                }
                modelMapper.map(inDTO, personsDTO);
                personsDTO.setUserName(usersDTOResult.getUserName());
                personsDTO.setPublicUserId(usersDTOResult.getPublicUserIdString());
                return personsDTO;
            } else {
                throw new RothisExceptionService(Errors.USER_ID_HAS_TAKEN.getMessage());
            }
        }
        return null;
    }

    private List<AddressesDTO> getAddressesDTOS(Long personId) {
        return addressesService.readAllAddressesByPersonId(personId);
    }

    private List<PhoneNumbersDTO> getPhoneNumbersDTOS(Long personId) {
        return phoneNumbersService.readAllPhoneNumbersByPersonId(personId);
    }

}
