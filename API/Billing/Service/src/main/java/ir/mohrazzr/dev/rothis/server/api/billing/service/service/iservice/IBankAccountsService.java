package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.BankAccountsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 12:17 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IBankAccountsService
 **/

public interface IBankAccountsService {
    BankAccountsDTO createBankAccount(BankAccountsDTO inDTO, String managerPublicUserId, String publicResourceId);
    BankAccountsDTO createBankAccountOwner(String managerPublicUserId, String bankAccountPublicId, String resourcePublicId, String newOwnerPublicId);
    BankAccountsDTO readBankAccount(String managerPublicUserId, String publicResourceId, String bankAccountPublicId);
    List<BankAccountsDTO> readAllBankAccount(String publicResourceId, String managerPublicUserId);
    BankAccountsDTO updateBankAccount(BankAccountsDTO inDTO, String managerPublicUserId, String publicResourceId, String bankAccountPublicId);
    BankAccountsDTO updateBankAccountOwner(String managerPublicUserId, String oldOwnerPublicId, String newOwnerPublicId, String bankAccountPublicId, String resourcePublicId);
    OperationStatusDTO deleteBankAccount(String managerPublicUserId, String publicResourceId, String bankAccountPublicId);
    boolean bankAccountExists(String resourcePublicId, String bankAccountPublicId);
    BankAccountsDTO findById(Long id);
}
