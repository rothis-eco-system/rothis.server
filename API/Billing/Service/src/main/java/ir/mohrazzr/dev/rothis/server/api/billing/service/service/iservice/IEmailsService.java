package ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice;

import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.EmailsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/07 - 8:40 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IEmailsService
 **/

public interface IEmailsService {
    EmailsDTO createEmail(EmailsDTO inDTO, String publicUserId);
    EmailsDTO readEmail(EmailsDTO inDTO, String publicUserId);
    EmailsDTO updateEmail(EmailsDTO inDTO, String publicUserId);
    OperationStatusDTO deleteEmail(EmailsDTO inDTO, String publicUserId);

}
