package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/16 - 12:52 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PortionOwnersDTO
 **/

public class PortionOwnersDTO {

    private Long id;
    private Long publicId;
    private String portionIdentifierCode;
    private String publicIdString;
    private Long resourceId;
    private Long personId;
    private Float portionValue;
    private Long portionSeconds;
    private PersonsDTO owner;

    public PortionOwnersDTO() {
    }

    public PortionOwnersDTO(Long id, Long publicId, String portionIdentifierCode, String publicIdString, Long resourceId, Long personId, Float portionValue, Long portionSeconds, PersonsDTO owner) {
        this.id = id;
        this.publicId = publicId;
        this.portionIdentifierCode = portionIdentifierCode;
        this.publicIdString = publicIdString;
        this.resourceId = resourceId;
        this.personId = personId;
        this.portionValue = portionValue;
        this.portionSeconds = portionSeconds;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPortionIdentifierCode() {
        return portionIdentifierCode;
    }

    public void setPortionIdentifierCode(String portionIdentifierCode) {
        this.portionIdentifierCode = portionIdentifierCode;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Float getPortionValue() {
        return portionValue;
    }

    public void setPortionValue(Float portionValue) {
        this.portionValue = portionValue;
    }

    public Long getPortionSeconds() {
        return portionSeconds;
    }

    public void setPortionSeconds(Long portionSeconds) {
        this.portionSeconds = portionSeconds;
    }

    public PersonsDTO getOwner() {
        return owner;
    }

    public void setOwner(PersonsDTO owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "PortionOwnersDTO{" +
                "id=" + id +
                ", publicId=" + publicId +
                ", portionIdentifierCode='" + portionIdentifierCode + '\'' +
                ", publicIdString='" + publicIdString + '\'' +
                ", resourceId=" + resourceId +
                ", personId=" + personId +
                ", portionValue=" + portionValue +
                ", portionSeconds=" + portionSeconds +
                ", owner=" + owner +
                '}';
    }
}
