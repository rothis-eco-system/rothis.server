package ir.mohrazzr.dev.rothis.server.api.billing.service.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.ResourceBillsEntity;
import ir.mohrazzr.dev.rothis.server.api.billing.domain.repository.ResourceBillsRepository;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationCode;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationName;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.enums.OperationStatus;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.*;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.utils.calculator.ResourcesCalculation;
import ir.mohrazzr.dev.rothis.server.api.utils.converter.Converter;
import ir.mohrazzr.dev.rothis.server.api.utils.generator.Generator;
import ir.mohrazzr.dev.rothis.server.api.utils.times.TimeUtil;
import ir.mohrazzr.dev.rothis.server.api.utils.UnitsUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/25 - 7:28 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourceBillsServiceImpl
 **/
@Service
public class ResourceBillsServiceImpl implements IResourceBillsService {
    private ResourceBillsRepository resourceBillsRepository;
    private IResourceManagerService resourceManagerService;
    private IResolverService idResolverService;
    private IPublicIDsService publicIDsService;
    private IBankAccountsService bankAccountsService;
    private IResourcesService resourcesService;
    private IBillsService billsService;
    private IPortionOwnersService ownersService;
    private TimeUtil timeUtil;
    private Converter converter;
    private ModelMapper modelMapper;
    private Generator generator;

    @Autowired
    public ResourceBillsServiceImpl(ResourceBillsRepository resourceBillsRepository, IResourceManagerService resourceManagerService, IResolverService idResolverService, IPublicIDsService publicIDsService, IBankAccountsService bankAccountsService, IResourcesService resourcesService, IBillsService billsService, IPortionOwnersService ownersService, Converter converter) {
        this.resourceBillsRepository = resourceBillsRepository;
        this.resourceManagerService = resourceManagerService;
        this.idResolverService = idResolverService;
        this.publicIDsService = publicIDsService;
        this.bankAccountsService = bankAccountsService;
        this.resourcesService = resourcesService;
        this.billsService = billsService;
        this.ownersService = ownersService;
        this.timeUtil = new TimeUtil();
        this.converter = converter;
        this.modelMapper = new ModelMapper();
        this.generator = new Generator();
    }

    @Override
    public ResourceBillsDTO createResourceBill(ResourceBillsDTO inDTO, String resourcePublicId, String managerPublicUserId, String bankAccountPublicId) {
        ResourceBillsDTO resourceBillsDTO = new ResourceBillsDTO();
        ResourceBillsEntity resourceBillsEntity = new ResourceBillsEntity();
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            if (bankAccountsService.bankAccountExists(resourcePublicId, bankAccountPublicId)) {
                Long exporterMangerId = idResolverService.getPersonID(managerPublicUserId);
                PublicIDsDTO resourceBillsPublicIDsDTO = publicIDsService.generatePublicId(35);
                ResourcesDTO resourcesDTO = resourcesService.readResource(managerPublicUserId, resourcePublicId);
                inDTO.setBankAccountId(idResolverService.getBankAccountID(bankAccountPublicId));
                inDTO.setResourceId(idResolverService.getResourceID(resourcePublicId));
                inDTO.setExportTime(new Timestamp(System.currentTimeMillis()));
                inDTO.setPricePerSecond(ResourcesCalculation.getPricePerPortion(resourcesDTO.getTotalResourceSeconds(), inDTO.getTotalPrice()));
                inDTO.setRoundCount(resourceBillsRepository.countByExporterManagerId(exporterMangerId) + 1);
                inDTO.setBillCount(resourceBillsRepository.countByResourceId(idResolverService.getResourceID(resourcePublicId)));
                inDTO.setPublicId(resourceBillsPublicIDsDTO.getId());
                inDTO.setResourceBillIdentityCode(generator.generateRandomNumber(8));
                while (resourceBillsRepository.findByResourceIdAndResourceBillIdentityCode(inDTO.getResourceId(), inDTO.getResourceBillIdentityCode()) != null) {
                    inDTO.setResourceBillIdentityCode(generator.generateRandomNumber(8));
                }
                timeUtil.setTimestamp(inDTO.getExportTime());
                timeUtil.setPaymentDeadLineDays(inDTO.getPaymentDeadLineDays());
                inDTO.setPaymentDeadLine(timeUtil.getPaymentDeadLine());
                modelMapper.getConfiguration().setAmbiguityIgnored(true);
                modelMapper.map(inDTO, resourceBillsEntity);
                resourceBillsEntity.setBankAccountId(inDTO.getBankAccountId());
                resourceBillsEntity.setExporterManagerId(exporterMangerId);
                ResourceBillsEntity resultResourceBillsEntity = resourceBillsRepository.save(resourceBillsEntity);
                if (resultResourceBillsEntity != null) {
                    resourceBillsDTO = getResourceBillsDTO(resourceBillsEntity, resourcePublicId, managerPublicUserId);
                    List<BillsDTO> billsDTOList = billsService.createBills(resourcePublicId, managerPublicUserId, resourceBillsDTO.getPublicIdString());
                    System.out.println(Arrays.toString(billsDTOList.toArray()));
                    if (!billsDTOList.isEmpty()) {
                        resourceBillsDTO.setBills(billsDTOList);
                        return resourceBillsDTO;
                    } else {
                        deleteResourceBill(resourcePublicId, managerPublicUserId, resourceBillsPublicIDsDTO.getPublicId());
                        throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage() + " : bills list is empty.");
                    }
                } else {
                    publicIDsService.deletePublicId(resourceBillsPublicIDsDTO.getId());
                    throw new RothisExceptionService(Errors.COULD_NOT_INSERT_RECORD.getMessage());
                }
            } else
                throw new RothisExceptionService(Errors.NOT_FOUND_BANK_ACCOUNT_WHIT_THIS_PUBLIC_ID_OR_RESOURCE_ID.getMessage());
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public ResourceBillsDTO readResourceBill(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId, String resourceBillIdentityCode) {
        if (!resourceBillPublicId.equals("null")) {
            return readResourceBillByPublicId(resourcePublicId, managerPublicUserId, resourceBillPublicId);
        } else if (!resourceBillIdentityCode.equals("null")) {
            return readResourceBillByIdentityCode(resourcePublicId, managerPublicUserId, resourceBillIdentityCode);
        } else throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage() + "isNull");
    }

    @Override
    public ResourceBillsDTO readResourceBillByPublicId(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndPublicId(
                    idResolverService.getResourceID(resourcePublicId), publicIDsService.findByPublicId(resourceBillPublicId).getId()
            );
            if (resourceBillsEntity != null) {
                return getResourceBillsDTO(resourceBillsEntity, resourcePublicId, managerPublicUserId);
            } else
                throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage());
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }


    @Override
    public ResourceBillsDTO readResourceBillByIdentityCode(String resourcePublicId, String managerPublicUserId, String resourceBillByIdentityCode) {
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndResourceBillIdentityCode(
                    idResolverService.getResourceID(resourcePublicId), resourceBillByIdentityCode
            );
            if (resourceBillsEntity != null) {
                return getResourceBillsDTO(resourceBillsEntity, resourcePublicId, managerPublicUserId);
            } else
                throw new RothisExceptionService(Errors.NO_RECORD_FOUND.getMessage());
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public ResourceBillsDTO updateResourceBill(ResourceBillsDTO inDTO, String resourcePublicId,
                                               String managerPublicUserId, String resourceBillPublicId,
                                               String bankAccountPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndPublicId(
                    idResolverService.getResourceID(resourcePublicId),
                    publicIDsService.findByPublicId(resourceBillPublicId).getId());
            if (resourceBillsEntity != null) {
                if (bankAccountsService.bankAccountExists(resourcePublicId, bankAccountPublicId)) {
                    Long exporterMangerId = idResolverService.getPersonID(managerPublicUserId);
                    PublicIDsDTO resourceBillsPublicIDsDTO = publicIDsService.updatePublicId(resourceBillPublicId);
                    inDTO.setBankAccountId(idResolverService.getBankAccountID(bankAccountPublicId));
                    inDTO.setExportTime(new Timestamp(System.currentTimeMillis()));
                    inDTO.setPricePerSecond(ResourcesCalculation.getPricePerPortion(
                            resourcesService.readResource(managerPublicUserId, resourcePublicId).getTotalResourceSeconds(),
                            inDTO.getTotalPrice()));
                    timeUtil.setTimestamp(inDTO.getExportTime());
                    timeUtil.setPaymentDeadLineDays(inDTO.getPaymentDeadLineDays());
                    inDTO.setPaymentDeadLine(timeUtil.getPaymentDeadLine());
                    resourceBillsEntity.setTotalPrice(inDTO.getTotalPrice());
                    resourceBillsEntity.setConcern(inDTO.getConcern());
                    resourceBillsEntity.setDescriptions(inDTO.getDescriptions());
                    resourceBillsEntity.setBankAccountId(inDTO.getBankAccountId());
                    resourceBillsEntity.setExporterManagerId(exporterMangerId);
                    resourceBillsEntity.setExportTime(inDTO.getExportTime());
                    resourceBillsEntity.setPublicId(inDTO.getPublicId());
                    resourceBillsEntity.setPaymentDeadLine(inDTO.getPaymentDeadLine());
                    resourceBillsEntity.setPricePerSecond(inDTO.getPricePerSecond());
                    resourceBillsEntity.setPublicId(resourceBillsPublicIDsDTO.getId());
                    ResourceBillsEntity resultResourceBillsEntity = resourceBillsRepository.save(resourceBillsEntity);
                    if (resultResourceBillsEntity != null) {
                        ResourceBillsDTO resourceBillsDTO = getResourceBillsDTO(resultResourceBillsEntity, resourcePublicId, managerPublicUserId);
                        List<BillsDTO> billsDTOList = billsService.updateBills(resourcePublicId, managerPublicUserId, resourceBillsDTO.getPublicIdString());
                        resourceBillsDTO.setBills(billsDTOList);
                        return resourceBillsDTO;
                    } else {
                        throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage());
                    }
                } else
                    throw new RothisExceptionService(Errors.NOT_FOUND_BANK_ACCOUNT_WHIT_THIS_PUBLIC_ID_OR_RESOURCE_ID.getMessage());
            } else
                throw new RothisExceptionService(Errors.COULD_NOT_UPDATE_RECORD.getMessage());
        } else
            throw new RothisExceptionService(Errors.NOT_FOUND_RESOURCE_WHIT_THIS_ID_OR_MANAGER.getMessage());
    }

    @Override
    public OperationStatusDTO deleteResourceBill(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId) {
        if (resourceManagerService.ifManagerOfResource(managerPublicUserId, resourcePublicId)) {
            ResourceBillsEntity resourceBillsEntity = resourceBillsRepository.findByResourceIdAndPublicId(
                    idResolverService.getResourceID(resourcePublicId),
                    publicIDsService.findByPublicId(resourceBillPublicId).getId()
            );
            if (resourceBillsEntity != null) {
                resourceBillsRepository.delete(resourceBillsEntity);
                return new OperationStatusDTO(OperationName.DELETE.name(),
                        OperationStatus.SUCCESS.name(), OperationCode.CODE200.name(),
                        Errors.DELETE_RECORD_SUCCESSFULLY.getMessage());
            }
        }
        return new OperationStatusDTO(OperationName.DELETE.name(),
                OperationStatus.ERROR.name(), OperationCode.CODE400.name(),
                Errors.COULD_NOT_DELETE_RECORD.getMessage());
    }

    @Override
    public List<ResourceBillsDTO> readAllResourceBills(String resourcePublicId, String managerPublicUserId, int page, int limit) {
        List<ResourceBillsDTO> resourceBillsDTOList = new ArrayList<>();
        Long resourceId = idResolverService.getResourceID(resourcePublicId);
        Pageable pageable = PageRequest.of(page, limit);
        Page<ResourceBillsEntity> resourceBillsEntityPage = resourceBillsRepository.findAllByResourceId(resourceId, pageable);
        List<ResourceBillsEntity> resourceBillsEntityList = resourceBillsEntityPage.getContent();
        for (ResourceBillsEntity resourceBillsEntity : resourceBillsEntityList) {
            resourceBillsDTOList.add(getResourceBillsDTO(resourceBillsEntity, resourcePublicId, managerPublicUserId));
        }
        return resourceBillsDTOList;
    }

    private ResourceBillsDTO getResourceBillsDTO(ResourceBillsEntity resourceBillsEntity, String resourcePublicId, String managerPublicUserId) {
        modelMapper = new ModelMapper();
        ResourceBillsDTO resourceBillsDTO = new ResourceBillsDTO();
        ResourcesDTO resourcesDTO = resourcesService.readResource(managerPublicUserId, resourcePublicId);
        modelMapper.map(resourceBillsEntity, resourceBillsDTO);
        resourceBillsDTO.setPublicIdString(publicIDsService.findById(resourceBillsEntity.getPublicId()).getPublicId());
        resourceBillsDTO.setBankAccount(bankAccountsService.findById(resourceBillsEntity.getBankAccountId()));
        resourceBillsDTO.setResourceIdString(resourcePublicId);
        resourceBillsDTO.setTotalPrice(UnitsUtil.fixDecimalValue(resourceBillsEntity.getTotalPrice()));
        resourceBillsDTO.setTotalPriceInAlphabet(converter.getParsedString(resourceBillsDTO.getTotalPrice().toString()).concat("ریال"));
        resourceBillsDTO.setPricePerSecond(UnitsUtil.fixDecimalValue(resourceBillsEntity.getPricePerSecond()));
        resourceBillsDTO.setPricePerPortion(UnitsUtil.fixDecimalValue(ResourcesCalculation.getPortionOwnerTotalPrice(resourceBillsEntity.getPricePerSecond(), resourcesDTO.getResourceSecondsPerPortion())));
        resourceBillsDTO.setPricePerPortionInAlphabet(converter.getParsedString(String.valueOf(resourceBillsDTO.getPricePerSecond().longValue())).concat("ریال"));
        resourceBillsDTO.setRoundCountInAlphabet(converter.getParsedString(resourceBillsDTO.getRoundCount().toString()).concat("م"));
        timeUtil.setTimestamp(resourceBillsDTO.getExportTime());
        resourceBillsDTO.setExportTimePersianDate(timeUtil.convertTimestampToPersianDate().getPersianDate().toString());
        timeUtil.setDate(resourceBillsDTO.getPaymentDeadLine());
        resourceBillsDTO.setPaymentDeadLinePersianDate(timeUtil.convertDateToPersianDate().getPersianDate().toString());
        List<BillsDTO> billsDTOList = billsService.readAllBills(resourcePublicId, managerPublicUserId, resourceBillsDTO.getPublicIdString());
        resourceBillsDTO.setBills(billsDTOList);
        return resourceBillsDTO;
    }

}
