package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/21 - 12:12 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BankAccountsDTO
 **/

public class BankAccountsDTO {
    private Long id;
    private Long publicId;
    private String publicIdString;
    private Long resourceId;
    private String resourcePublicId;
    private String bankName;
    private String accountNumber;
    private String name;
    private String shabaNumber;
    private String cardNumber;
    private String branchCode;
    private String branchName;
    private String branchAddress;
    private String description;
    private List<String> oldOwners;
    private List<String> ownerList;
    private List<PersonsDTO> owners;

    public BankAccountsDTO() {
    }

    public BankAccountsDTO(Long id, Long publicId, String publicIdString, Long resourceId, String resourcePublicId, String bankName, String accountNumber, String name, String shabaNumber, String cardNumber, String branchCode, String branchName, String branchAddress, String description, List<String> oldOwners, List<String> ownerList, List<PersonsDTO> owners) {
        this.id = id;
        this.publicId = publicId;
        this.publicIdString = publicIdString;
        this.resourceId = resourceId;
        this.resourcePublicId = resourcePublicId;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.name = name;
        this.shabaNumber = shabaNumber;
        this.cardNumber = cardNumber;
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.description = description;
        this.oldOwners = oldOwners;
        this.ownerList = ownerList;
        this.owners = owners;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourcePublicId() {
        return resourcePublicId;
    }

    public void setResourcePublicId(String resourcePublicId) {
        this.resourcePublicId = resourcePublicId;
    }

    public List<PersonsDTO> getOwners() {
        return owners;
    }

    public void setOwners(List<PersonsDTO> owners) {
        this.owners = owners;
    }

    @Override
    public String toString() {
        return "BankAccountsDTO{" +
                "id=" + id +
                ", publicId=" + publicId +
                ", publicIdString='" + publicIdString + '\'' +
                ", resourceId=" + resourceId +
                ", resourcePublicId='" + resourcePublicId + '\'' +
                ", bankName='" + bankName + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", name='" + name + '\'' +
                ", shabaNumber='" + shabaNumber + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", branchName='" + branchName + '\'' +
                ", branchAddress='" + branchAddress + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public List<String> getOldOwners() {
        return oldOwners;
    }

    public void setOldOwners(List<String> oldOwners) {
        this.oldOwners = oldOwners;
    }

    public List<String> getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(List<String> ownerList) {
        this.ownerList = ownerList;
    }
}
