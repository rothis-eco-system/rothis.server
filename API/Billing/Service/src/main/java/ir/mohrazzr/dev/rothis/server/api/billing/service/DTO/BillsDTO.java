package ir.mohrazzr.dev.rothis.server.api.billing.service.DTO;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/12/01 - 4:13 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BillsDTO
 **/

public class BillsDTO {
    private Long id;
    private Long publicId;
    private String publicIdString;
    private Long resourceBillsId;
    private String resourceBillsPublicIdString;
    private Long portionOwnerId;
    private String portionOwnerPublicId;
    private PersonsDTO owner;
    private Timestamp createdAt;
    private String createdAtPersianDate;
    private BigDecimal billPrice;
    private String billPriceInAlphabet;
    private String billIdentityCode;
    private Integer billNumber;

    public BillsDTO() {
    }

    public BillsDTO(Long id, Long publicId, String publicIdString, Long resourceBillsId, String resourceBillsPublicIdString, Long portionOwnerId, String portionOwnerPublicId, PersonsDTO owner, Timestamp createdAt, String createdAtPersianDate, BigDecimal billPrice, String billPriceInAlphabet, String billIdentityCode, Integer billNumber) {
        this.id = id;
        this.publicId = publicId;
        this.publicIdString = publicIdString;
        this.resourceBillsId = resourceBillsId;
        this.resourceBillsPublicIdString = resourceBillsPublicIdString;
        this.portionOwnerId = portionOwnerId;
        this.portionOwnerPublicId = portionOwnerPublicId;
        this.owner = owner;
        this.createdAt = createdAt;
        this.createdAtPersianDate = createdAtPersianDate;
        this.billPrice = billPrice;
        this.billPriceInAlphabet = billPriceInAlphabet;
        this.billIdentityCode = billIdentityCode;
        this.billNumber = billNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return publicId;
    }

    public void setPublicId(Long publicId) {
        this.publicId = publicId;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public Long getResourceBillsId() {
        return resourceBillsId;
    }

    public void setResourceBillsId(Long resourceBillsId) {
        this.resourceBillsId = resourceBillsId;
    }

    public String getResourceBillsPublicIdString() {
        return resourceBillsPublicIdString;
    }

    public void setResourceBillsPublicIdString(String resourceBillsPublicIdString) {
        this.resourceBillsPublicIdString = resourceBillsPublicIdString;
    }

    public Long getPortionOwnerId() {
        return portionOwnerId;
    }

    public void setPortionOwnerId(Long portionOwnerId) {
        this.portionOwnerId = portionOwnerId;
    }

    public String getPortionOwnerPublicId() {
        return portionOwnerPublicId;
    }

    public void setPortionOwnerPublicId(String portionOwnerPublicId) {
        this.portionOwnerPublicId = portionOwnerPublicId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAtPersianDate() {
        return createdAtPersianDate;
    }

    public void setCreatedAtPersianDate(String createdAtPersianDate) {
        this.createdAtPersianDate = createdAtPersianDate;
    }

    public BigDecimal getBillPrice() {
        return billPrice;
    }

    public void setBillPrice(BigDecimal billPrice) {
        this.billPrice = billPrice;
    }

    public String getBillPriceInAlphabet() {
        return billPriceInAlphabet;
    }

    public void setBillPriceInAlphabet(String billPriceInAlphabet) {
        this.billPriceInAlphabet = billPriceInAlphabet;
    }

    public String getBillIdentityCode() {
        return billIdentityCode;
    }

    public void setBillIdentityCode(String billIdentityCode) {
        this.billIdentityCode = billIdentityCode;
    }

    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    public PersonsDTO getOwner() {
        return owner;
    }

    public void setOwner(PersonsDTO owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "BillsDTO{" +
                "id=" + id +
                ", publicId=" + publicId +
                ", publicIdString='" + publicIdString + '\'' +
                ", resourceBillsId=" + resourceBillsId +
                ", resourceBillsPublicIdString='" + resourceBillsPublicIdString + '\'' +
                ", portionOwnerId=" + portionOwnerId +
                ", portionOwnerPublicId='" + portionOwnerPublicId + '\'' +
                ", owner=" + owner +
                ", createdAt=" + createdAt +
                ", createdAtPersianDate='" + createdAtPersianDate + '\'' +
                ", billPrice=" + billPrice +
                ", billPriceInAlphabet='" + billPriceInAlphabet + '\'' +
                ", billIdentityCode='" + billIdentityCode + '\'' +
                ", billNumber=" + billNumber +
                '}';
    }
}