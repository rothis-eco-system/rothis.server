package ir.mohrazzr.dev.rothis.server.api.billing.reporter.service.impl;

import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.reporter.model.ResourceBillsReportModel;
import ir.mohrazzr.dev.rothis.server.api.billing.reporter.model.ResourceBillsReportResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.reporter.service.iservice.IReportResourceBillsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.*;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPersonsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPortionOwnersService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourceBillsService;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourcesService;
import ir.mohrazzr.dev.rothis.server.api.utils.converter.Converter;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/31 - 10:32 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ReportService
 **/
@Service
public class ReportResourceBillsServiceImpl implements IReportResourceBillsService {
    private IResourceBillsService resourceBillsService;
    private IResourcesService resourcesService;
    private IPortionOwnersService portionOwnersService;
    private IPersonsService personsService;
    private Converter converter = new Converter();
    private NumberFormat formatter = new DecimalFormat("#,###");

    @Autowired
    public ReportResourceBillsServiceImpl(IResourceBillsService resourceBillsService, IResourcesService resourcesService, IPortionOwnersService portionOwnersService, IPersonsService personsService) {
        this.resourceBillsService = resourceBillsService;
        this.resourcesService = resourcesService;
        this.portionOwnersService = portionOwnersService;
        this.personsService = personsService;
    }

    @Override
    public ResourceBillsReportResponse generateResourceBillsListReport(String resourcePublicId, String managerPublicUserId, String resourceBillPublicId, String resourceBillIdentityCode, String reportTemplatesPath, String generatedReportPath, String billIdentityCode, String message) {
        try {
            ResourceBillsDTO resourceBillsDTO = resourceBillsService.readResourceBill(resourcePublicId, managerPublicUserId, resourceBillPublicId, resourceBillIdentityCode);
            ResourcesDTO resourcesDTO = resourcesService.readResource(managerPublicUserId, resourceBillsDTO.getResourceIdString());
            PersonsDTO exporterPersonalDetails = personsService.findPersonByPublicUserId(managerPublicUserId);
            List<ResourceBillsReportModel> resourceBillsReportModelList = new ArrayList<>();
            long sumDayLightSeconds = 0L;
            BigDecimal sumBillsPrice = new BigDecimal("0");
            float sumOwnersPortionValues = 0;
            for (BillsDTO bill : resourceBillsDTO.getBills()) {
                ResourceBillsReportModel reportModel = new ResourceBillsReportModel();
                PortionOwnersDTO portionOwnersDTO = portionOwnersService.findById(bill.getPortionOwnerId());
                reportModel.setResourceName(resourcesDTO.getName());
                reportModel.setResourceBillIdentityCode(resourceBillsDTO.getResourceBillIdentityCode());
                reportModel.setPaymentDeadLine(resourceBillsDTO.getPaymentDeadLine().toString());
                reportModel.setPaymentDeadLinePersianDate(resourceBillsDTO.getPaymentDeadLinePersianDate().replace("-", "/"));
                reportModel.setTotalPrice(formatter.format(resourceBillsDTO.getTotalPrice()));
                reportModel.setPricePerSecond(resourceBillsDTO.getPricePerSecond());
                reportModel.setPricePerPortion(resourceBillsDTO.getPricePerPortion());
                reportModel.setTotalPriceInAlphabet(resourceBillsDTO.getTotalPriceInAlphabet());
                reportModel.setPricePerPortionInAlphabet(resourceBillsDTO.getPricePerPortionInAlphabet());
                reportModel.setBillCount(resourceBillsDTO.getBillCount());
                reportModel.setRoundCount(resourceBillsDTO.getRoundCount());
                reportModel.setRoundCountInAlphabet(resourceBillsDTO.getRoundCountInAlphabet());
                reportModel.setExportTimePersianDate(resourceBillsDTO.getExportTimePersianDate().replace("-", "/"));
                reportModel.setConcern(resourceBillsDTO.getConcern());
                reportModel.setDescriptions(resourceBillsDTO.getDescriptions());
                reportModel.setCircuitOfTheDay(resourcesDTO.getCircuitOfTheDay());
                reportModel.setCreatedAt(bill.getCreatedAt().toString());
                reportModel.setCreatedAtPersianDate((bill.getCreatedAtPersianDate().replace("-", "/")));
                reportModel.setBillPrice(bill.getBillPrice());
                reportModel.setBillPriceInAlphabet(bill.getBillPriceInAlphabet());
                reportModel.setBillIdentityCode(bill.getBillIdentityCode());
                reportModel.setBillNumber(bill.getBillNumber());
                reportModel.setOwnerPortionValue(portionOwnersDTO.getPortionValue());
                reportModel.setOwnerPortionValueDayLightTime(converter.calculateDayLightTime(portionOwnersDTO.getPortionSeconds()));
                sumDayLightSeconds = sumDayLightSeconds + portionOwnersDTO.getPortionSeconds();
                reportModel.setSumDayLightTime(converter.calculateDayLightTime(sumDayLightSeconds));
                reportModel.setUserName(bill.getOwner().getUserName());
                reportModel.setFirstName(bill.getOwner().getFirstName());
                reportModel.setLastName(bill.getOwner().getLastName());
                reportModel.setFatherName(bill.getOwner().getFatherName());
                reportModel.setNationalCode(bill.getOwner().getNationalCode());
                reportModel.setPassPortCode(bill.getOwner().getPassPortCode());
                reportModel.setGender(bill.getOwner().getGender());
                reportModel.setDateOfBirth(bill.getOwner().getDateOfBirth());

                for (AddressesDTO address : bill.getOwner().getAddresses()) {
                    if (address.getPrimary()) {
                        reportModel.setPostalCode(address.getPostalCode());
                        reportModel.setAddress(address.getAddress());
                    }
                }

                for (PhoneNumbersDTO phoneNumber : bill.getOwner().getPhoneNumbers()) {
                    if (phoneNumber.getPrimary())
                        reportModel.setPhoneNumber(phoneNumber.getPhoneNumber());
                }

                reportModel.setBankName(resourceBillsDTO.getBankAccount().getBankName());
                reportModel.setAccountNumber(resourceBillsDTO.getBankAccount().getAccountNumber());
                reportModel.setAccountName(resourceBillsDTO.getBankAccount().getName());
                reportModel.setShabaNumber(resourceBillsDTO.getBankAccount().getShabaNumber());
                reportModel.setCardNumber(resourceBillsDTO.getBankAccount().getCardNumber());
                reportModel.setBranchCode(resourceBillsDTO.getBankAccount().getBranchCode());
                reportModel.setBranchName(resourceBillsDTO.getBankAccount().getBranchName());
                reportModel.setBranchAddress(resourceBillsDTO.getBankAccount().getBranchAddress());
                reportModel.setDescription(resourceBillsDTO.getBankAccount().getDescription());
                sumBillsPrice = sumBillsPrice.add(bill.getBillPrice());
                reportModel.setSumBillPrice(sumBillsPrice);
                sumOwnersPortionValues = sumOwnersPortionValues + portionOwnersDTO.getPortionValue();
                reportModel.setSumOwnerPortionValue(sumOwnersPortionValues);
                reportModel.setExporterNameFamily(exporterPersonalDetails.getFirstName().concat(" ").concat(exporterPersonalDetails.getLastName()));
                reportModel.setResourceBillMessage(message);
                resourceBillsReportModelList.add(reportModel);
            }
            JasperReport jasperReport = JasperCompileManager
                    .compileReport(reportTemplatesPath);
            JRBeanCollectionDataSource jrBeanCollectionDataSource = null;
            if (billIdentityCode.isEmpty() || billIdentityCode.isBlank()) {
                jrBeanCollectionDataSource = new JRBeanCollectionDataSource(resourceBillsReportModelList);
            } else {
                for (ResourceBillsReportModel reportModel : resourceBillsReportModelList) {
                    if (reportModel.getBillIdentityCode().equals(billIdentityCode)) {
                        System.out.println("founded");
                        jrBeanCollectionDataSource = new JRBeanCollectionDataSource(Collections.singleton(reportModel));
                        break;
                    }
                }
            }
            Map<String, Object> parameters = new HashMap<String, Object>();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint,
                    generatedReportPath);
            return new ResourceBillsReportResponse(generatedReportPath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RothisExceptionService("Report not generated :" + e.getMessage());
        }
    }
}
