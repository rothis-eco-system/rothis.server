package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.UserDetailsRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.UserDetailsResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.UsersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IUsersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES.USERS_URL;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 10:50 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UsersController
 **/
@RestController
@RequestMapping(value = USERS_URL)
public class UsersController {
    private final IUsersService userService;

    @Autowired
    public UsersController(IUsersService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/{id}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserDetailsResponse getUser(@PathVariable String id) {
        UserDetailsResponse response = new UserDetailsResponse();
        UsersDTO dto = userService.findUserByPublicUserId(id);
        BeanUtils.copyProperties(dto, response);
        return response;
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserDetailsResponse createUser(@RequestBody UserDetailsRequest userDetails) throws Exception {
        UserDetailsResponse response = new UserDetailsResponse();
        UsersDTO dto = new UsersDTO();
        BeanUtils.copyProperties(userDetails, dto);
        UsersDTO createdUser = userService.createUser(dto);
        BeanUtils.copyProperties(createdUser, response);
        return response;
    }

    @PostMapping(path = "/new",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserDetailsResponse createUser(@RequestBody UserDetailsRequest userDetails, @RequestParam(value = "role", defaultValue = "") Integer role) throws Exception {
        UserDetailsResponse response = new UserDetailsResponse();
        UsersDTO dto = new UsersDTO();
        if (role > 2) {
            dto.setUserRole(role);
        } else
            dto.setUserRole(404);
        BeanUtils.copyProperties(userDetails, dto);
        UsersDTO createdUser = userService.createUser(dto);
        BeanUtils.copyProperties(createdUser, response);
        return response;
    }

    @PutMapping(path = "/{id}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserDetailsResponse updateUser(@PathVariable String id, @RequestBody UserDetailsRequest userDetails, @RequestParam(value = "role") Integer role) throws Exception {
        UserDetailsResponse response = new UserDetailsResponse();
        UsersDTO dto = new UsersDTO();
        BeanUtils.copyProperties(userDetails, dto);
        if (role >= 2) {
            dto.setUserRole(role);
        } else
            dto.setUserRole(404);
        UsersDTO updateUser = userService.updateUser(id, dto);
        BeanUtils.copyProperties(updateUser, response);
        return response;
    }

    @DeleteMapping(path = "/{id}")
    public OperationStatusResponse deleteUser(@PathVariable String id) throws Exception {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = userService.deleteUser(id);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }

    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<UserDetailsResponse> getAllUsers(@RequestParam(value = "page") int page, @RequestParam(value = "limit") int limit) {
        List<UserDetailsResponse> userDetailsResponseList = new ArrayList<>();
        if (page > 0) page -= 1;
        List<UsersDTO> dtoList = userService.readAllUsers(page, limit);
        for (UsersDTO usersDTO : dtoList) {
            UserDetailsResponse userDetailsResponse = new UserDetailsResponse();
            BeanUtils.copyProperties(usersDTO, userDetailsResponse);
            userDetailsResponseList.add(userDetailsResponse);
        }
        return userDetailsResponseList;
    }

}
