package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/15 - 9:48 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesResponse
 **/

public class ResourcesResponse {
    private String publicIdString;
    private String name;
    private String identifierCode;
    private Float totalResourcePortion;
    private Float totalUnallocatedResourcePortion;
    private Integer dongValue;
    private Long totalResourceSeconds;
    private Long resourceSecondsPerPortion;
    private Integer circuitOfTheDay;

    public ResourcesResponse() {
    }

    public ResourcesResponse(String publicIdString, String name, String identifierCode, Float totalResourcePortion, Float totalUnallocatedResourcePortion, Integer dongValue, Long totalResourceSeconds, Long resourceSecondsPerPortion, Integer circuitOfTheDay) {
        this.publicIdString = publicIdString;
        this.name = name;
        this.identifierCode = identifierCode;
        this.totalResourcePortion = totalResourcePortion;
        this.totalUnallocatedResourcePortion = totalUnallocatedResourcePortion;
        this.dongValue = dongValue;
        this.totalResourceSeconds = totalResourceSeconds;
        this.resourceSecondsPerPortion = resourceSecondsPerPortion;
        this.circuitOfTheDay = circuitOfTheDay;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifierCode() {
        return identifierCode;
    }

    public void setIdentifierCode(String identifierCode) {
        this.identifierCode = identifierCode;
    }

    public Float getTotalResourcePortion() {
        return totalResourcePortion;
    }

    public void setTotalResourcePortion(Float totalResourcePortion) {
        this.totalResourcePortion = totalResourcePortion;
    }

    public Float getTotalUnallocatedResourcePortion() {
        return totalUnallocatedResourcePortion;
    }

    public void setTotalUnallocatedResourcePortion(Float totalUnallocatedResourcePortion) {
        this.totalUnallocatedResourcePortion = totalUnallocatedResourcePortion;
    }

    public Integer getDongValue() {
        return dongValue;
    }

    public void setDongValue(Integer dongValue) {
        this.dongValue = dongValue;
    }

    public Long getTotalResourceSeconds() {
        return totalResourceSeconds;
    }

    public void setTotalResourceSeconds(Long totalResourceSeconds) {
        this.totalResourceSeconds = totalResourceSeconds;
    }

    public Long getResourceSecondsPerPortion() {
        return resourceSecondsPerPortion;
    }

    public void setResourceSecondsPerPortion(Long resourceSecondsPerPortion) {
        this.resourceSecondsPerPortion = resourceSecondsPerPortion;
    }

    public Integer getCircuitOfTheDay() {
        return circuitOfTheDay;
    }

    public void setCircuitOfTheDay(Integer circuitOfTheDay) {
        this.circuitOfTheDay = circuitOfTheDay;
    }

}
