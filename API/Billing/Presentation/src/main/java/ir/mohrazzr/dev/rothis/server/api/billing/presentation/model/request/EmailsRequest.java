package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/11/07 - 8:28 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : EmailsRequest
 **/

public class EmailsRequest {
    private String publicUserId;
    private String email;
    private Boolean isPrimary;

    public EmailsRequest() {
    }

    public EmailsRequest(String publicUserId, String email, Boolean isPrimary) {
        this.publicUserId = publicUserId;
        this.email = email;
        this.isPrimary = isPrimary;
    }

    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }
}
