package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.PhoneNumbersRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.PhoneNumbersResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PhoneNumbersDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPhoneNumbersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/13 - 7:39 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PhoneNumbersController
 **/
@RestController
@RequestMapping(value = ROUTES.PERSONS_PHONE_NUMBERS_URL)
public class PhoneNumbersController {
    private IPhoneNumbersService phoneNumbersService;

    @Autowired
    public PhoneNumbersController(IPhoneNumbersService iPhoneNumbersService) {
        this.phoneNumbersService = iPhoneNumbersService;
    }

    @PostMapping(path = "/{publicUserId}/phoneNumbers",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PhoneNumbersResponse createPhoneNumber(@RequestBody PhoneNumbersRequest phoneNumbersRequest, @PathVariable(value = "publicUserId") String publicUserId) throws Exception {
        PhoneNumbersResponse response = new PhoneNumbersResponse();
        PhoneNumbersDTO phoneNumbersDTO = new PhoneNumbersDTO();
        BeanUtils.copyProperties(phoneNumbersRequest, phoneNumbersDTO);
        PhoneNumbersDTO resultDTO = phoneNumbersService.createPhoneNumber(phoneNumbersDTO, publicUserId);
        BeanUtils.copyProperties(resultDTO, response);
        return response;
    }

    @GetMapping(path = "/{publicUserId}/phoneNumbers/{publicAddressId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PhoneNumbersResponse readPhoneNumber(@PathVariable(value = "publicUserId") String publicUserId, @PathVariable(value = "publicAddressId") String publicAddressId) {
        PhoneNumbersResponse phoneNumbersResponse = new PhoneNumbersResponse();
        PhoneNumbersDTO phoneNumbersDTO = phoneNumbersService.readPhoneNumber(publicUserId, publicAddressId);
        BeanUtils.copyProperties(phoneNumbersDTO, phoneNumbersResponse);
        return phoneNumbersResponse;
    }
    @GetMapping(path = "/{publicUserId}/phoneNumbers", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<PhoneNumbersResponse> getAllPersons(@PathVariable(name = "publicUserId") String publicUserId) {
        List<PhoneNumbersResponse> PhoneNumbersResponseList = new ArrayList<>();
        List<PhoneNumbersDTO> phoneNumbersDTOList = phoneNumbersService.readAllPhoneNumbersByPublicUserId(publicUserId);
        if (phoneNumbersDTOList!=null&&!phoneNumbersDTOList.isEmpty()){
            for (PhoneNumbersDTO PhoneNumbersDTO :phoneNumbersDTOList){
                PhoneNumbersResponse PhoneNumbersResponse = new PhoneNumbersResponse();
                BeanUtils.copyProperties(PhoneNumbersDTO,PhoneNumbersResponse);
                PhoneNumbersResponseList.add(PhoneNumbersResponse);
            }
            return PhoneNumbersResponseList;
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_PERSON_ADDRESS.getMessage());
    }
    @PutMapping(path = "/{publicUserId}/phoneNumbers/{phoneNumberPublicId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PhoneNumbersResponse updatePhoneNumber(@RequestBody PhoneNumbersRequest phoneNumbersRequest, @PathVariable(value = "publicUserId") String publicUserId ,@PathVariable(value = "phoneNumberPublicId") String phoneNumberPublicId) throws Exception {
        PhoneNumbersResponse response = new PhoneNumbersResponse();
        PhoneNumbersDTO phoneNumbersDTO = new PhoneNumbersDTO();
        BeanUtils.copyProperties(phoneNumbersRequest, phoneNumbersDTO);
        PhoneNumbersDTO resultDTO = phoneNumbersService.updatePhoneNumber(phoneNumbersDTO, publicUserId,phoneNumberPublicId);
        BeanUtils.copyProperties(resultDTO, response);
        return response;
    }

    @DeleteMapping(path = "/{publicUserId}/phoneNumbers/{publicPhoneNumberId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public OperationStatusResponse deletePhoneNumber(@PathVariable(value = "publicUserId") String publicUserId, @PathVariable(value = "publicPhoneNumberId") String publicPhoneNumberId) {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = phoneNumbersService.deletePhoneNumber(publicUserId, publicPhoneNumberId);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }
}
