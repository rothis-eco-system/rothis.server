package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.PersonsExtraRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.PersonsRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.PersonsExtraResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.PersonsResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.PersonsDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IPersonsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/08 - 10:41 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PersonsController
 **/
@RestController
@RequestMapping(path = ROUTES.PERSONS_URL)
public class PersonsController {
    private IPersonsService personsService;

    public PersonsController(IPersonsService personsService) {
        this.personsService = personsService;
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(path = "/{publicUserId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsExtraResponse createPerson(@RequestBody PersonsExtraRequest personsRequest, @PathVariable(value = "publicUserId") String publicUserId) throws Exception {
        PersonsExtraResponse response = new PersonsExtraResponse();
        PersonsDTO personsDTO = new PersonsDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsRequest, personsDTO);
        PersonsDTO resultDTO = personsService.createPerson(personsDTO, publicUserId);
        modelMapper.map(resultDTO, response);
        return response;
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(path = "/new/with_contact_info/with_role",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsExtraResponse createPerson(@RequestBody PersonsExtraRequest personsRequest, @RequestParam(value = "userRole") Integer userRole) throws Exception {
        PersonsExtraResponse response = new PersonsExtraResponse();
        PersonsDTO personsDTO = new PersonsDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsRequest, personsDTO);
        PersonsDTO resultDTO = personsService.createPerson(personsDTO, userRole);
        modelMapper.map(resultDTO, response);
        return response;
    }

    @PostMapping(path = "/new/with_contact_info",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsExtraResponse createPerson(@RequestBody PersonsExtraRequest personsRequest) throws Exception {
        PersonsExtraResponse response = new PersonsExtraResponse();
        PersonsDTO personsDTO = new PersonsDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsRequest, personsDTO);
        PersonsDTO resultDTO = personsService.createPerson(personsDTO);
        modelMapper.map(resultDTO, response);
        return response;
    }

    @GetMapping(path = "/{publicUserId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsResponse readPerson(@PathVariable(value = "publicUserId") String publicUserId, String s) {
        PersonsResponse personsResponse = new PersonsResponse();
        PersonsDTO personsDTO = personsService.readPerson(publicUserId);
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsDTO, personsResponse);
        return personsResponse;
    }

    @GetMapping(path = "/{publicUserId}/with_contact_info", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsExtraResponse readPerson(@PathVariable(value = "publicUserId") String publicUserId) {
        PersonsExtraResponse personsExtraResponse = new PersonsExtraResponse();
        PersonsDTO personsDTO = personsService.readPerson(publicUserId);
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsDTO, personsExtraResponse);
        return personsExtraResponse;
    }

    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<PersonsExtraResponse> readAllPerson(@RequestParam(value = "page") int page, @RequestParam(value = "limit") int limit) {
        List<PersonsExtraResponse> personsExtraResponseList = new ArrayList<>();
        List<PersonsDTO> personsDTOList = personsService.readAllPersons(page, limit);
        ModelMapper modelMapper = new ModelMapper();
        for (PersonsDTO personsDTO : personsDTOList) {
            PersonsExtraResponse personsExtraResponse = new PersonsExtraResponse();
            modelMapper.map(personsDTO, personsExtraResponse);
            personsExtraResponseList.add(personsExtraResponse);
        }
        return personsExtraResponseList;
    }

    @PutMapping(path = "/{publicUserId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PersonsResponse updatePerson(@RequestBody PersonsRequest personsRequest, @PathVariable(value = "publicUserId") String publicUserId) throws Exception {
        PersonsResponse response = new PersonsResponse();
        PersonsDTO personsDTO = new PersonsDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(personsRequest, personsDTO);
        PersonsDTO resultDTO = personsService.updatePerson(personsDTO, publicUserId);
        modelMapper.map(resultDTO, response);
        return response;
    }


    @DeleteMapping(path = "/{publicUserId}")
    public OperationStatusResponse deletePerson(@PathVariable String publicUserId) throws Exception {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = personsService.deletePerson(publicUserId);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }


}
