package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/10/31 - 5:05 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : OpreationStatus
 **/

public enum OperationStatus {
    ERROR, SUCCESS
}
