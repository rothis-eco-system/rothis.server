package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.ResourcesRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.ResourcesResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.ResourcesDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IResourcesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Friday in 2019/11/15 - 9:52 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ResourcesController
 **/
@RestController
@RequestMapping(path = ROUTES.RESOURCES_URL)
public class ResourcesController {
    private IResourcesService resourcesService;

    @Autowired
    public ResourcesController(IResourcesService resourcesService) {
        this.resourcesService = resourcesService;
    }

    @PostMapping(path = "/new/{publicUserId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourcesResponse createResource(@RequestBody ResourcesRequest resourcesRequest, @PathVariable(value = "publicUserId") String publicUserId) throws Exception {
        ResourcesResponse response = new ResourcesResponse();
        ResourcesDTO dto = new ResourcesDTO();
        BeanUtils.copyProperties(resourcesRequest, dto);
        ResourcesDTO resultResourcesDTO = resourcesService.createResource(dto, publicUserId);
        BeanUtils.copyProperties(resultResourcesDTO, response);
        return response;
    }

    @GetMapping(path = "/{publicResourceId}/{managerPublicUserId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourcesResponse readResource(@PathVariable(name = "managerPublicUserId") String managerPublicUserId, @PathVariable(name = "publicResourceId") String publicResourceId) {
        ResourcesResponse resourcesResponse = new ResourcesResponse();
        ModelMapper modelMapper = new ModelMapper();
        ResourcesDTO resourcesDTO = resourcesService.readResource(managerPublicUserId, publicResourceId);
        modelMapper.map(resourcesDTO, resourcesResponse);
        return resourcesResponse;
    }
    @GetMapping(path = "/{managerPublicUserId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<ResourcesResponse> readAllResource(@PathVariable(name = "managerPublicUserId") String managerPublicUserId) {
        List<ResourcesResponse> resourcesResponseList = new ArrayList<>();
        List<ResourcesDTO> resourcesDTOList = resourcesService.readAllResourcesByManagerPublicId(managerPublicUserId);
        for (ResourcesDTO resourcesDTO : resourcesDTOList) {
            ResourcesResponse response = new ResourcesResponse();
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.map(resourcesDTO,response);
            resourcesResponseList.add(response);
        }
        return resourcesResponseList;
    }
    @PutMapping(path = "/{publicResourceId}/{managerPublicUserId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResourcesResponse updateResource(@RequestBody ResourcesRequest resourcesRequest, @PathVariable(value = "managerPublicUserId") String managerPublicUserId, @PathVariable(value = "publicResourceId") String publicResourceid) {
        ResourcesResponse resourcesResponse = new ResourcesResponse();
        ResourcesDTO resourcesDTO = new ResourcesDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(resourcesRequest, resourcesDTO);
        ResourcesDTO resultResourcesDTO = resourcesService.updateResource(resourcesDTO, managerPublicUserId, publicResourceid);
        modelMapper.map(resultResourcesDTO, resourcesResponse);
        return resourcesResponse;
    }

    @DeleteMapping(path = "/{publicResourceId}/{managerPublicUserId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public OperationStatusResponse deleteResource(@PathVariable(value = "managerPublicUserId") String managerPublicUserId, @PathVariable(value = "publicResourceId") String publicResourceid) {
        OperationStatusResponse operationStatusResponse = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = resourcesService.deleteResource(managerPublicUserId, publicResourceid);
        BeanUtils.copyProperties(operationStatusDTO, operationStatusResponse);
        return operationStatusResponse;
    }
}