package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/18 - 8:24 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PortionOwnersRequest
 **/

public class PortionOwnersRequest {
    private Float portionValue;

    public PortionOwnersRequest() {
    }

    public PortionOwnersRequest(Float portionValue) {
        this.portionValue = portionValue;
    }

    public Float getPortionValue() {
        return portionValue;
    }

    public void setPortionValue(Float portionValue) {
        this.portionValue = portionValue;
    }
}
