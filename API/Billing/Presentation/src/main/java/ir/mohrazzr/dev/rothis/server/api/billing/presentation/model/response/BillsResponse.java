package ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/12/01 - 4:24 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BillsResponse
 **/

public class BillsResponse {
    private String publicIdString;
    private String resourceBillsPublicIdString;
    private String portionOwnerPublicId;
    private PersonsResponse owner;
    private Timestamp createdAt;
    private String createdAtPersianDate;
    private BigDecimal billPrice;
    private String billPriceInAlphabet;
    private String billIdentityCode;
    private Integer billNumber;

    public BillsResponse() {
    }

    public BillsResponse(String publicIdString, String resourceBillsPublicIdString, String portionOwnerPublicId, PersonsResponse owner, Timestamp createdAt, String createdAtPersianDate, BigDecimal billPrice, String billPriceInAlphabet, String billIdentityCode, Integer billNumber) {
        this.publicIdString = publicIdString;
        this.resourceBillsPublicIdString = resourceBillsPublicIdString;
        this.portionOwnerPublicId = portionOwnerPublicId;
        this.owner = owner;
        this.createdAt = createdAt;
        this.createdAtPersianDate = createdAtPersianDate;
        this.billPrice = billPrice;
        this.billPriceInAlphabet = billPriceInAlphabet;
        this.billIdentityCode = billIdentityCode;
        this.billNumber = billNumber;
    }

    public String getPublicIdString() {
        return publicIdString;
    }

    public void setPublicIdString(String publicIdString) {
        this.publicIdString = publicIdString;
    }

    public String getResourceBillsPublicIdString() {
        return resourceBillsPublicIdString;
    }

    public void setResourceBillsPublicIdString(String resourceBillsPublicIdString) {
        this.resourceBillsPublicIdString = resourceBillsPublicIdString;
    }

    public String getPortionOwnerPublicId() {
        return portionOwnerPublicId;
    }

    public void setPortionOwnerPublicId(String portionOwnerPublicId) {
        this.portionOwnerPublicId = portionOwnerPublicId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAtPersianDate() {
        return createdAtPersianDate;
    }

    public void setCreatedAtPersianDate(String createdAtPersianDate) {
        this.createdAtPersianDate = createdAtPersianDate;
    }

    public BigDecimal getBillPrice() {
        return billPrice;
    }

    public void setBillPrice(BigDecimal billPrice) {
        this.billPrice = billPrice;
    }

    public String getBillPriceInAlphabet() {
        return billPriceInAlphabet;
    }

    public void setBillPriceInAlphabet(String billPriceInAlphabet) {
        this.billPriceInAlphabet = billPriceInAlphabet;
    }

    public String getBillIdentityCode() {
        return billIdentityCode;
    }

    public void setBillIdentityCode(String billIdentityCode) {
        this.billIdentityCode = billIdentityCode;
    }

    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    public PersonsResponse getOwner() {
        return owner;
    }

    public void setOwner(PersonsResponse owner) {
        this.owner = owner;
    }
}
