package ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.rest;

import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.request.AddressesRequest;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.AddressesResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.model.response.opreations.OperationStatusResponse;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.handler.RothisExceptionService;
import ir.mohrazzr.dev.rothis.server.api.billing.exception.message.Errors;
import ir.mohrazzr.dev.rothis.server.api.billing.presentation.controller.router.ROUTES;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.AddressesDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.DTO.OperationStatusDTO;
import ir.mohrazzr.dev.rothis.server.api.billing.service.service.iservice.IAddressesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA at Saturday in 2019/11/09 - 5:40 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : AddressesController
 **/
@RestController
@RequestMapping(value = ROUTES.PERSON_ADDRESSES)
public class AddressesController {
    private IAddressesService addressesService;

    @Autowired
    public AddressesController(IAddressesService iAddressesService) {
        this.addressesService = iAddressesService;
    }

    @PostMapping(path = "/{publicUserId}/addresses",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public AddressesResponse createAddresses(@RequestBody AddressesRequest addressesRequest, @PathVariable(value = "publicUserId") String publicUserId) throws Exception {
        AddressesResponse response = new AddressesResponse();
        AddressesDTO addressesDTO = new AddressesDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(addressesRequest, addressesDTO);
        AddressesDTO resultDTO = addressesService.createAddress(addressesDTO, publicUserId);
        modelMapper.map(resultDTO, response);
        return response;
    }

    @GetMapping(path = "/{publicUserId}/addresses/{publicAddressId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public AddressesResponse readAddress(@PathVariable(value = "publicUserId") String publicUserId, @PathVariable(value = "publicAddressId") String publicAddressId) {
        AddressesResponse addressesResponse = new AddressesResponse();
        AddressesDTO addressesDTO = addressesService.readAddress(publicUserId, publicAddressId);
        BeanUtils.copyProperties(addressesDTO, addressesResponse);
        return addressesResponse;
    }

    @GetMapping(path = "/{publicUserId}/addresses", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<AddressesResponse> getAllAddresses(@PathVariable(name = "publicUserId") String publicUserId) {
        List<AddressesResponse> addressesResponseList = new ArrayList<>();
        List<AddressesDTO> addressesDTOList = addressesService.readAllAddressesByPublicUserId(publicUserId);
        if (addressesDTOList!=null&&!addressesDTOList.isEmpty()){
            for (AddressesDTO addressesDTO :addressesDTOList){
            AddressesResponse addressesResponse = new AddressesResponse();
            BeanUtils.copyProperties(addressesDTO,addressesResponse);
            addressesResponseList.add(addressesResponse);
            }
            return addressesResponseList;
        }
        throw new RothisExceptionService(Errors.NOT_FOUND_PERSON_ADDRESS.getMessage());
    }

    @PutMapping(path = "/{publicUserId}/addresses/{publicAddressId}",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public AddressesResponse updateAddress(@RequestBody AddressesRequest addressesRequest, @PathVariable(value = "publicUserId") String publicUserId, @PathVariable(value = "publicAddressId") String publicAddressId) throws Exception {
        AddressesResponse response = new AddressesResponse();
        AddressesDTO addressesDTO = new AddressesDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(addressesRequest, addressesDTO);
        AddressesDTO resultDTO = addressesService.updateAddress(addressesDTO, publicUserId, publicAddressId);
        modelMapper.map(resultDTO, response);
        return response;
    }

    @DeleteMapping(path = "/{publicUserId}/addresses/{publicAddressId}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public OperationStatusResponse deleteAddress(@PathVariable(value = "publicUserId") String publicUserId, @PathVariable(value = "publicAddressId") String publicAddressId) {
        OperationStatusResponse response = new OperationStatusResponse();
        OperationStatusDTO operationStatusDTO = addressesService.deleteAddress(publicUserId, publicAddressId);
        BeanUtils.copyProperties(operationStatusDTO, response);
        return response;
    }
}
