package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_public_ids")
@Data
public class PublicIDsEntity implements Serializable {
  private static final long serialVersionUID = 4981046039527308791L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;
  @Column(name = "public_id", nullable = false,length = 100,unique = true)
  private String publicId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }
}