package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BankAccountOwnersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountOwnersRepository extends JpaRepository<BankAccountOwnersEntity, Long>, JpaSpecificationExecutor<BankAccountOwnersEntity> {
    BankAccountOwnersEntity findByUserIdAndBankAccountId(Long userId, Long bankAccountId);
    List<BankAccountOwnersEntity> findAllByBankAccountId(Long bankAccountId);
}