package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.PhoneNumbersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneNumbersRepository extends JpaRepository<PhoneNumbersEntity, Integer>, JpaSpecificationExecutor<PhoneNumbersEntity> {
    List<PhoneNumbersEntity> findAllByPersonsId(Long personId);
    PhoneNumbersEntity findByPersonsId(Long publicUserId);
    PhoneNumbersEntity findByPersonsIdAndPublicId(Long personsId, Long publicId);
    PhoneNumbersEntity findByPublicId(Long id);
}