package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BillsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillsRepository extends JpaRepository<BillsEntity, Double>, JpaSpecificationExecutor<BillsEntity> {
    List<BillsEntity> findAllByResourceBillsId(Long resourceBillsId);
}