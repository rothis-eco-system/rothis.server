package ir.mohrazzr.dev.rothis.server.api.billing.domain.repository;

import ir.mohrazzr.dev.rothis.server.api.billing.domain.entity.BankAccountsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountsRepository extends JpaRepository<BankAccountsEntity, Long>, JpaSpecificationExecutor<BankAccountsEntity> {
    BankAccountsEntity findByPublicId(Long publicId);
    List<BankAccountsEntity>findAllByResourceId(Long resourceId);
    BankAccountsEntity findByAccountNumberOrCardNumber(String accountNumber, String cardNumber);

}