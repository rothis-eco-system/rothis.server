package ir.mohrazzr.dev.rothis.server.api.billing.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Table(name = "tbl_bills")
@Entity
public class BillsEntity implements Serializable {
  private static final long serialVersionUID = 2278584122540708638L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;
  @Column(name = "public_id")
  private Long publicId ;
  @Column(name = "resource_bills_id", nullable = false)
  private Long resourceBillsId;
  @Column(name = "portion_owner_id", nullable = false)
  private Long portionOwnerId;
  @Column(name = "createdAt", nullable = false)
  private Timestamp createdAt;
  @Column(name = "bill_price", nullable = false)
  private BigDecimal billPrice;
  @Column(name = "bill_identity_code", nullable = false ,length = 12)
  private String billIdentityCode;
  @Column(name = "bill_number")
  private Integer billNumber;
}