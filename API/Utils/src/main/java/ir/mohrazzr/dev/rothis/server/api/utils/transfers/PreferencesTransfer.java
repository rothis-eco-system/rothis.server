package ir.mohrazzr.dev.rothis.server.api.utils.transfers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by IntelliJ IDEA at Monday in 2019/11/04 - 6:48 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : PreferencesTransfer
 **/

public class PreferencesTransfer {
    private static Logger logger = LoggerFactory.getLogger(PreferencesTransfer.class);

    private static Preferences preferences;

    public static String getPreferencesValue(String key, Class<?> cls) {
        boolean checked = false;
        String value = null;
        preferences = Preferences.userNodeForPackage(cls);
        value = preferences.get(key, value);
        if (value != null && !value.isEmpty() && !value.isBlank()) {
            checked = true;
        }
        return Objects.requireNonNull(value);
    }

    public static void setPreferencesValue(String key, String value, Class<?> cls) {
        Preferences pref = Preferences.userNodeForPackage(cls);
        pref.put(key, value);

        preferences = pref;
    }

    public static void clearPreferencesValue(String key, Class<?> cls) {
        Preferences pref = Preferences.userNodeForPackage(cls);
        try {
            pref.clear();
            preferences = pref;
        } catch (BackingStoreException e) {
            logger.error(e.toString());
        }
    }

}
