package ir.mohrazzr.dev.rothis.server.api.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by IntelliJ IDEA at Monday in 2019/12/30 - 5:01 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UnitsUtil
 **/

public  class UnitsUtil {
    public static BigDecimal fixDecimalValue(BigDecimal inputDecimal) {
        DecimalFormat decimalFormat = new DecimalFormat("###.##");
        return new BigDecimal(decimalFormat.format(inputDecimal));
    }
}
