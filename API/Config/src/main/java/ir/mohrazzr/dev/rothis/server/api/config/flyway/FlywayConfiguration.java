package ir.mohrazzr.dev.rothis.server.api.config.flyway;

import ir.mohrazzr.dev.rothis.server.api.config.mysql.MysqlConfiguration;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/10/30 - 9:26 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : FlywayConfiguration
 **/
@Configuration
@ConditionalOnClass(DataSource.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@EnableTransactionManagement
@PropertySource(value = "classpath:properties/flyway.properties")
@EnableJpaRepositories(value = "ir.mohrazzr.dev.rothis.server")
public class FlywayConfiguration {
    private final MysqlConfiguration mysqlConfiguration;
    private final Environment env;

    @Autowired
    public FlywayConfiguration( Environment env) {
        this.mysqlConfiguration = new MysqlConfiguration(env);
        this.env = env;
    }


    @Bean(initMethod = "migrate")
    Flyway flyway() {
        ClassicConfiguration configuration = new ClassicConfiguration();
        configuration.setDataSource(mysqlConfiguration.dataSource());
        configuration.setLocationsAsStrings(env.getProperty("rothis.datasource.flyway.migrations.location"));
        Flyway flyway = new Flyway(configuration);
        flyway.baseline();
        flyway.migrate();
        return flyway;
    }

    @Bean
    @ConditionalOnBean
    @ConditionalOnMissingBean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    @ConditionalOnBean
    @ConditionalOnMissingBean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
